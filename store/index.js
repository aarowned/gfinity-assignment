export const state = () => ({
	username: 'steveroesler'
})

export const mutations = {
	updateSetting(state, { name, value }) {
		state.settings[name].selected = value
	}
}
